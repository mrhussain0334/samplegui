$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});

function changeType(e) {
    $("#typeCustomer").removeClass("btn-success btn-default").addClass("btn-default");
    $("#typePharmacist").removeClass("btn-success btn-default").addClass("btn-default");
    $(e).removeClass('btn-default').addClass('btn-success');
}