function initMap() {
    var uluru = {lat: 31.584, lng: 73.344};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "Assets/img/logo-32.png"
    });
    var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h4 id="firstHeading" class="firstHeading">Pharmacy Name</h4>'+
        '<div id="bodyContent">'+
        '<p>Details of Pharmacy</p>' +
        '</div>'+
        '</div>';

    var infoWindow = new google.maps.InfoWindow({
        content: contentString
    });
    marker.addListener('click', function() {
        infoWindow.open(map, marker);
    });

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            marker.setPosition(pos);
            infoWindow.setContent(contentString);
            map.setCenter(pos);

        }, function() {

        });
    } else {
        // Browser doesn't support Geolocation
    }
}


